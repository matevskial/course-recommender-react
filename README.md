# Course recommender

## Overview

This repository contains the project for the course Knowledge-based systems in my uni.

It is written in React.

Main features are:
    
* Recommend a course based on answered questions
* Show a list of already recommended courses
* Remove already recommended courses from list

The decision tree is implemented using JavaScript objects\
and manipulated by hand-written functions.

The two screenshots below shows questions to be answered by user

<img src="documentation/screenshots/displayQuestionScreen1.png" width="80%" height="80%" title="Question screen" alt="Question screen 1">

<img src="documentation/screenshots/displayQuestionScreen2.png" width="80%" height="80%" title="Question screen" alt="Question screen 2">

The screenshot below shows a recommended course based on the answered questions

<img src="documentation/screenshots/displayRecommendedCourseScreen.png" width="80%" height="80%" title="Recommended course screen" alt="Recommended course screen">

## Available Scripts to start the project

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
