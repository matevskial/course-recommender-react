import React from 'react';

import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import ExpertSystem from "./components/ExpertSystem";

import 'materialize-css/dist/css/materialize.min.css';
import './styles/main.css';

import {decisionTree} from "./expertsystem/decisionTree";


const App = () => {

    return (
        <div className={"teal accent-1"}>
            <Navbar />
            <ExpertSystem decisionTree={decisionTree} />
            <Footer />
        </div>

    );
}

export default App;