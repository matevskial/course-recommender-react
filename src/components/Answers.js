import React, {Fragment, useState} from 'react';
import MaterializeSelect from "../materialize-wrapped-components/MaterializeSelect";

const Answers = (props) => {

    const { currentNode } = props;
    const { applyAnswer } = props.callbacks;

    const [answerNode, setAnswerNode] = useState(null);
    const [answerIndex, setAnswerIndex] = useState(null);

    const setAnswerNodeWithIndex = (index) => {
        index = parseInt(index);
        setAnswerNode(currentNode.answers.answers[index].node);
        setAnswerIndex(index);
    }
    const displayAnswers = () => {
        if(currentNode.answers.inputType === "buttons") {
            return currentNode.answers.answers.map((answer, index) => {
                return (answer.node.marked === undefined || answer.node.marked === false) && (
                    <div className={""} onClick={() => applyAnswer(answer.node, currentNode, index)}>
                        <div className={"btnX waves-effect waves-light btn-large"}>{answer.label}</div>
                    </div>
                );
            });
        } else if(currentNode.answers.inputType === "combo") {
            const options = currentNode.answers.answers.map((answer, index) => {
               return (answer.node.marked === undefined || answer.node.marked === false) && (
                   <option value={index}>{answer.label}</option>
               )
            });

            const confirmSelectionButton = answerNode != null &&
                (<div className={"col offset-m3 l4 offset-l4"} onClick={() => applyAnswer(answerNode, currentNode, answerIndex)}>
                    <div className={"btnX waves-effect waves-light btn-large"}>Следно</div>
                </div>)

            return (
                <Fragment>
                    <MaterializeSelect disabledOptionText={"Изберете"} options={options} callbacks={{setOption: setAnswerNodeWithIndex}}/>
                    {confirmSelectionButton}
                </Fragment>

            );
        }
    }

    return (
        <div id="answers-container" className="row">
            {displayAnswers()}
        </div>
    );
}

export default Answers;