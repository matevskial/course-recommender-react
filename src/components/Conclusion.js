import React, {Fragment, useEffect} from 'react';

const Conclusion = (props) => {

    const { currentNode } = props;
    const { restartExpertSystem, addRecommendedCourse, dismissRecommendedCourse } = props.callbacks;

    useEffect(() => {
        addRecommendedCourse(currentNode.conclusion);
    }, [currentNode.conclusion])

    return (
        <Fragment>
            <h3 className="white-text">
                Препорачан изборен предмет
            </h3>
            <p className="flow-text white-text">
                {currentNode.conclusion}
            </p>
            <p className="flow-text white-text">
                Кликнете на копчето за да започнете одново
            </p>
            <div className="btn btn-floating btn-large white conclusion-button" onClick={restartExpertSystem}>
                <i className="material-icons teal-text">check</i>
            </div>


            <div className="btn btn-floating btn-large white conclusion-button" onClick={dismissRecommendedCourse} >
                <i className="material-icons teal-text">arrow_back</i>
            </div>
        </Fragment>
    );
}

export default Conclusion;