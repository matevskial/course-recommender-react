import React, {Fragment, useEffect, useState} from 'react';

import Question from "./Question";
import Answers from "./Answers";
import WelcomeBanner from "./WelcomeBanner";
import Conclusion from "./Conclusion";

import {hasOnlyOneUnmarked, markPath, unmarkPath} from '../expertsystem/decisionTree';

const ExpertSystem = (props) => {

    const {decisionTree} = props;

    const [currentNode, setCurrentNode] = useState(null);
    const [interviewState, setInterviewState] = useState("welcome");
    const [decisionTreePath, setDecisionTreePath] = useState([]);
    const [recommendedCourses, setRecommendedCourses] = useState([]);

    useEffect(() => {
        startExpertSystem();
    }, []);

    const startExpertSystem = () => {
        setCurrentNode(decisionTree);
        setInterviewState("welcome");
        setDecisionTreePath([]);
    }

    const startInterview = () => {
        setInterviewState(currentNode.type);
    }

    const applyAnswer = (nextNode, prevNode, prevNodeAnswerIndex) => {
        setCurrentNode(nextNode);
        setInterviewState(nextNode.type);
        setDecisionTreePath([...decisionTreePath, {node: prevNode, answerIndex: prevNodeAnswerIndex}]);
    }

    const addRecommendedCourse = (course) => {
        setRecommendedCourses([...recommendedCourses, {course, path: decisionTreePath}]);
        markPath(decisionTreePath);
    }
    const deleteRecommendedCourse = (index) => {
        let {path} = recommendedCourses[index];
        let result = recommendedCourses.filter((_, i) => index !== i);

        setRecommendedCourses(result);
        unmarkPath(path);
    }

    const goToPreviousNode = () => {
        if(decisionTreePath.length >= 1) {
            let deleteIndex = decisionTreePath.length - 1;
            while(deleteIndex > 0) {
                let canDelete = hasOnlyOneUnmarked(decisionTreePath[deleteIndex].node);
                if(!canDelete) {
                    break;
                }
                deleteIndex--;
            }
            const prev = decisionTreePath[deleteIndex];
            const prevNode = prev.node;
            setCurrentNode(prevNode);
            setInterviewState(prevNode.type);
            const newDecisionPath = decisionTreePath.filter((_, index) => index < deleteIndex);
            setDecisionTreePath(newDecisionPath);
        }
    }

    const dismissRecommendedCourse = () => {
        if(recommendedCourses.length >= 1) {
            deleteRecommendedCourse(recommendedCourses.length - 1);
            goToPreviousNode();
        }
    }

    const displayBackButton = () => {
        return decisionTreePath.length >= 1 && (
            <div className="btn btn-floating btn-large white back-button" onClick={goToPreviousNode}>
                <i className="material-icons teal-text">arrow_back</i>
            </div>
        );
    }

    const display = () => {
        if(interviewState === "welcome") {
            return (
                <WelcomeBanner startInterview={startInterview} />
            );
        } else if(interviewState === "root" || interviewState === "interview") {
            return (
                <Fragment>
                    {displayBackButton()}
                    <Question currentNode={currentNode} />
                    <Answers currentNode={currentNode} callbacks={{applyAnswer}} />
                </Fragment>
            );
        } else {
            return (
                <Conclusion currentNode={currentNode} callbacks={{restartExpertSystem: startExpertSystem, addRecommendedCourse, dismissRecommendedCourse}} />
            );
        }
    }

    const displayRecommendedCourses = () => {
        return (
            <div className="collection">
                {recommendedCourses.map((recommendation, index) => {
                    return <a className="collection-item">{recommendation.course}<a href="#!" onClick={() => deleteRecommendedCourse(index)} className="secondary-content"><i className="material-icons">delete</i></a></a>
                })}
            </div>
        )
    }

    return (
        <main className="section-expert teal accent-1 center">

            <div className="container">
                <div className="row">
                    <div className="card-panel teal">
                        {display()}
                    </div>
                </div>

                <div className="row">
                    <div className="card-panel teal">
                        <p id="welcome-message" className="flow-text white-text">
                            Досега препорачани курсеви
                        </p>
                        {displayRecommendedCourses()}
                    </div>
                </div>
            </div>
        </main>
    );
}

export default ExpertSystem;