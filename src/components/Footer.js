import React from 'react';

const Footer = () => {
    return (
        <footer className="page-footer teal darken-1">
            <div className="container">
                <div className="row">
                    <div className="col l6 s12">
                        <h5 className="white-text">Проект изработен од:</h5>
                        <p className="grey-text text-lighten-4">
                            Александар Матевски ИНКИ361
                        </p>
                        <p className="grey-text text-lighten-4">
                           Дизајн на страна: Димитар Наумоски ИКТ9
                        </p>
                        <h5 className="white-text">Предмет:</h5>
                        <p className="grey-text text-lighten-4">Системи на знаење</p>
                    </div>
                    <div className="col l4 offset-l2 s12">
                        <h5 className="white-text">Ментор:</h5>
                        <p className="grey-text text-lighten-4">д-р Наташа Б. Табаковска</p>
                        <h5 className="white-text">Универзитет:</h5>
                        <p className="grey-text text-lighten-4">
                            ,,Св. Климент Охридски'' - Битола
                        </p>
                    </div>
                </div>
            </div>
            <div className="footer-copyright teal darken-3">
                <div className="container">
                    © 2021 Сите права задржани
                    <a className="grey-text text-lighten-4 right" href="https://www.fikt.uklo.edu.mk/" rel="noreferrer" target="_blank">Факултет
                        за информатички и комуникациски технологии</a>
                </div>
            </div>
        </footer>
    )
}

export default Footer;