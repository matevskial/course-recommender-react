import React from "react";

const Navbar = () => {
    return (
        <nav className="teal darken-1">
            <div className="nav-wrapper">
                <div className={"row"}>
                    <div className={"col s12"}>
                        <div className="brand-logo hide-on-small-only">Советувач за изборни предмети за ФИКТ - ИНКИ 2015/2016</div>
                    </div>
                </div>


            </div>
        </nav>
    );
}

export default Navbar;