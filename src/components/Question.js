import React from 'react';

const Question = (props) => {

    const { currentNode } = props;

    return (
        <p id="question-container" className="flow-text white-text">
            {currentNode.question}
        </p>
    );
}

export default Question;