import React from 'react';

const WelcomeBanner = (props) => {

    const { startInterview } = props;

    return (
        <div>
            <p id="welcome-message" className="flow-text white-text">
                Кликнете на копчето за да започните
            </p>

            <div className="btn btn-floating btn-large pulse white start-button" onClick={startInterview}>
                <i className="material-icons teal-text">play_arrow</i>
            </div>

        </div>
    )
}

export default WelcomeBanner;
