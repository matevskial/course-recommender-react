import texts from './texts';

const semesterOne = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се воведам во концептот на ИКТ инженерство", node: {type: "conclusion", conclusion: texts.courses["INKI-105"]}},
            {label: "Да научам повеќе за модерното информатичко општество", node: {type: "conclusion", conclusion: texts.courses["INKI-106"]}}
        ]
    }
}

const semesterTwo = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со практични знаења и вештини", node: {type: "conclusion", conclusion: texts.courses["INKI-205"]}},
            {label: "Да научам повеќе за модерното информатичко општество", node: {type: "conclusion", conclusion: texts.courses["INKI-206"]}}
        ]
    }
}

const semesterThree = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со теоретски знаења од областа на математиката", node: {type: "conclusion", conclusion: texts.courses["INKI-305"]}},
            {label: "Да се здобијам со знаења за организациски, деловни и претприемачки вештини",
                node: {
                    type: "interview",
                    question: "Избери еден од понудените предмети: ",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: texts.courses["INKI-306"], node: {type: "conclusion", conclusion: texts.courses["INKI-306"]}},
                            {label: texts.courses["INKI-307"], node: {type: "conclusion", conclusion: texts.courses["INKI-307"]}}
                        ]
                    }
                }
            }
        ]
    }
}

const semesterFour = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со теоретски знаења од областа на математиката", node: {type: "conclusion", conclusion: texts.courses["INKI-405"]}},
            {label: "Да се здобијам со знаења за организациски, деловни и претприемачки вештини", node: {type: "conclusion", conclusion: texts.courses["INKI-408"]}},
            {label: "Да ги усовршам моите лидерски способности", node: {type: "conclusion", conclusion: texts.courses["INKI-409"]}},
        ]
    }
}

const semesterFive = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со знаења за компјутерски мрежи", node: {type: "conclusion", conclusion: texts.courses["INKI-512"]}},
            {label: "Да се здобијам со практични знаења и вештини",
                node: {
                    type: "interview",
                    question: "Избери област за која сакаш да се здобиеш со практични знаења",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: "Видео игри", node: {type: "conclusion", conclusion: texts.courses["INKI-511"]}},
                            {label: "Компјутерски мрежи", node: {type: "conclusion", conclusion: texts.courses["INKI-513"]}},
                            {label: "Програмирање", node: {type: "conclusion", conclusion: texts.courses["INKI-508"]}}
                        ]
                    }
                }
                },
            {label: "Да се здобијам со теоретски знаења од областа на компјутерски науки", node: {type: "conclusion", conclusion: texts.courses["INKI-514"]}},
        ]
    }
}

const semesterSix = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со теоретски знаења од областа на компјутерски науки", node: {type: "conclusion", conclusion: texts.courses["INKI-610"]}},
            {label: "Да се здобијам со практични знаења и вештини",
                node: {
                    type: "interview",
                    question: "Избери област за која сакаш да се здобиеш со практични знаења",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: "Бази на податоци", node: {type: "conclusion", conclusion: texts.courses["INKI-604"]}},
                            {label: "Видео игри", node: {
                                    type: "interview",
                                    question: "Избери еден од понудените предмети: ",
                                    answers: {
                                        inputType: "buttons",
                                        answers: [
                                            {label: texts.courses["INKI-607"], node: {type: "conclusion", conclusion: texts.courses["INKI-607"]}},
                                            {label: texts.courses["INKI-608"], node: {type: "conclusion", conclusion: texts.courses["INKI-608"]}}
                                        ]
                                    }
                                }},
                        ]
                    }
                }
            }
        ]
    }
}

const semesterSeven = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со практични знаења и вештини",
                node: {
                    type: "interview",
                    question: "Избери област за која сакаш да се здобиеш со практични знаења",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: "3D Моделирање", node: {type: "conclusion", conclusion: texts.courses["INKI-701"]}},
                            {label: "Вештачка интелигенција", node: {type: "conclusion", conclusion: texts.courses["INKI-714"]}},
                            {label: "Компјутерски мрежи", node: {type: "conclusion", conclusion: texts.courses["INKI-702"]}},
                        ]
                    }
                }
            },

            {label: "Да се здобијам со знаења за организациски, деловни и претприемачки вештини", node: {type: "conclusion", conclusion: texts.courses["INKI-719"]}},
            {label: "Да се здобијам со теоретски знаења од областа на компјутерски науки", node: {
                    type: "interview",
                    question: "Избери област за која сакаш да се здобиеш со знаења од областа на компјутерски науки",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: "Бази на податоци", node: {type: "conclusion", conclusion: texts.courses["INKI-715"]}},
                            {label: "Криптографија", node: {type: "conclusion", conclusion: texts.courses["INKI-705"]}},

                        ]
                    }
                }},
        ]
    }
}

const semesterEight = {
    type: "interview",
    question: "Мојата цел за избирање на предмет е:",
    answers: {
        inputType: "combo",
        answers: [
            {label: "Да се здобијам со практични знаења и вештини",
                node: {
                    type: "interview",
                    question: "Избери област за која сакаш да се здобиеш со практични знаења",
                    answers: {
                        inputType: "buttons",
                        answers: [
                            {label: "Програмирање", node: {type: "conclusion", conclusion: texts.courses["INKI-812"]}},
                            {label: "Вештачка интелигенција", node: {type: "conclusion", conclusion: texts.courses["INKI-806"]}}
                        ]
                    }
                }
            },

            {label: "Да се здобијам со знаења од применета информатика", node: {type: "conclusion", conclusion: texts.courses["INKI-805"]}},
            {label: "Да се здобијам со знаења за ИС(информациски системи) во организациите", node: {type: "conclusion", conclusion: texts.courses["INKI-814"]}},

        ]
    }
}

const markPath = (path) => {
    for(let i = path.length - 1; i >= 0; i--) {
        const {node, answerIndex} = path[i];
        if(node.type === "root") {
           break;
        }

        const nodeToMark = node.answers.answers[answerIndex].node;

        let markedNodes = 0;
        const answers = nodeToMark.answers ? nodeToMark.answers.answers : [];
        for(const answer of answers) {
            const n = answer.node;
            if(n.marked) {
                markedNodes++;
            }
        }

        if(markedNodes === answers.length) {
            nodeToMark.marked = true;
        } else {
            break;
        }
    }
}

const unmarkPath = (path) => {
    for(let i = path.length - 1; i >= 0; i--) {
        const {node, answerIndex} = path[i];
        if(node.type === "root") {
            break;
        }

        const nodeToUnmark = node.answers.answers[answerIndex].node;

        let markedNodes = 0;
        const answers = nodeToUnmark.answers ? nodeToUnmark.answers.answers : [];
        for(const answer of answers) {
            const n = answer.node;
            if(n.marked) {
                markedNodes++;
            }
        }

        if(markedNodes !== answers.length || markedNodes === 0) {
            nodeToUnmark.marked = false;
        } else {
            break;
        }
    }
}

const hasOnlyOneUnmarked = (node) => {
    let markedNodes = 0;
    const answers = node.answers ? node.answers.answers : [];
    for(const answer of answers) {
        const n = answer.node;
        if(n.marked) {
            markedNodes++;
        }
    }

    return answers.length - markedNodes <= 1;
}

const decisionTree = {
    type: "root",
    question: "За кој семестар сакаш да избереш предмети?",
    answers: {
        inputType: "buttons",
        answers: [
            {label: "1 - прв семестар", node: semesterOne},
            {label: "2 - втор семестар", node: semesterTwo},
            {label: "3 - трет семестар", node: semesterThree},
            {label: "4 - четврт семестар", node: semesterFour},
            {label: "5 - петти семестар", node: semesterFive},
            {label: "6 - шести семестар", node: semesterSix},
            {label: "7 - седми семестар", node: semesterSeven},
            {label: "8 - осми семестар", node: semesterEight}
        ]
    }
};

export  {decisionTree, markPath, unmarkPath, hasOnlyOneUnmarked};