import React, {useEffect} from 'react';
import M from 'materialize-css';

/**
 * This components wraps  the element select because for material-css, it is needed to execute
 * additional code to initialize the select, that code is placed in useEffect
 *
 * @returns {JSX}
 * @constructor
 */
const MaterializeSelect = (props) => {
    const {setOption} = props.callbacks;
    const { disabledOptionText, options } = props;

    useEffect(() => {
        let selects = document.querySelectorAll('select');
        M.FormSelect.init(selects, {});
    }, [])

    return (
        <div className="input-field col s12">
            <select onChange={(event) => setOption(event.target.value)}>
                <option value="" disabled selected>{disabledOptionText}</option>
                {options}
            </select>
        </div>
    );
}

export default MaterializeSelect;